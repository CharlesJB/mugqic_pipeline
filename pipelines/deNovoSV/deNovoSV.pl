#!/usr/bin/perl

=head1 NAME

I<deNovoSV>

=head1 SYNOPSIS

deNovoSV.pl B<args> [-f -c -n -s -e]

=head1 DESCRIPTION

B<deNovoSV> Is the main de novo Structural Variation pipeline.

=head1 AUTHORS

B<Eric Audemard> - I<eric.audemard@mail.mcgill.ca>

